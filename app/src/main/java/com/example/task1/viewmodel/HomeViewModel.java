package com.example.task1.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.task1.model.User;
import com.example.task1.repo.Repository;

import java.util.List;

public class HomeViewModel extends AndroidViewModel {
    private Application applicationContext;
    private Repository repository;


    public HomeViewModel(@NonNull Application application) {
        super(application);
        applicationContext = application;
        repository = new Repository(applicationContext);
    }

    public LiveData<List<User>> getUsersList() {
        return repository.getAllUsers();
    }


    public User getUser(String email, String password) {
        return repository.getUser(email, password);
    }

    public void updateUserContactsList(User user) {
        repository.updateUserContactsList(user);
    }
}
