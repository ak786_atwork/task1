package com.example.task1.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.database.sqlite.SQLiteConstraintException;
import android.support.annotation.NonNull;

import com.example.task1.R;
import com.example.task1.helper.Validator;
import com.example.task1.model.User;
import com.example.task1.repo.Repository;

import java.util.regex.Pattern;

public class LoginSignupViewModel extends AndroidViewModel {

    private Application applicationContext;
    private Repository repository;

    private MutableLiveData<Boolean> isDataValid = new MutableLiveData<>();
    private MutableLiveData<String> errors = new MutableLiveData<>();
    private User user;

    public LoginSignupViewModel(@NonNull Application application) {
        super(application);
        applicationContext = application;
        repository = new Repository(applicationContext);
    }

    public void validateAndInsert(String email, String password) {
        boolean isEmailValid = Validator.isEmailValid(email);
        boolean isPasswordValid = Validator.isPasswordValid(password);

        StringBuilder sb = new StringBuilder();

        if (!isEmailValid && !isPasswordValid) {
            sb.append(applicationContext.getString(R.string.invalid_password) + "\n");
            sb.append(applicationContext.getString(R.string.invalid_email));
        } else if (!isEmailValid) {
            sb.append(applicationContext.getString(R.string.invalid_email));
        } else if (!isPasswordValid) {
            sb.append(applicationContext.getString(R.string.invalid_password) + "\n");
        } else {
            user = repository.getUser(email, password);
            if (user != null) {
                //show successful toast
                isDataValid.postValue(true);
                return;
            } else {
                sb.append(sb.append(applicationContext.getString(R.string.user_not_exists) + "\n"));
            }
        }
        errors.postValue(sb.toString());
    }

    public void insertUser(User user) throws SQLiteConstraintException {
        repository.insert(user);
    }

    public MutableLiveData<Boolean> getIsDataValid() {
        return isDataValid;
    }

    public void setIsDataValid(MutableLiveData<Boolean> isDataValid) {
        this.isDataValid = isDataValid;
    }

    public MutableLiveData<String> getErrors() {
        return errors;
    }

    public void setErrors(MutableLiveData<String> errors) {
        this.errors = errors;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
