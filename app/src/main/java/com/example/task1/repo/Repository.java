package com.example.task1.repo;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

import com.example.task1.model.AppDatabase;
import com.example.task1.model.User;
import com.example.task1.model.UserDao;

import java.util.List;

public class Repository {
    private AppDatabase appDatabase;
    private UserDao userDao;

    public Repository(Context context) {
        appDatabase = AppDatabase.getDatabase(context);
        userDao = appDatabase.userDao();
    }

    public LiveData<List<User>> getAllUsers() {
        return userDao.getAll();
    }

    public void insert(User user) throws SQLiteConstraintException {
         userDao.insert(user);
    }

    public User getUser(String email, String password) {
        return userDao.getUserByEmailAndPassword(email,password);
    }

    public void updateUserContactsList(User user) {
        userDao.update(user);
    }
}
