package com.example.task1.view;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.task1.R;
import com.example.task1.helper.Validator;
import com.example.task1.model.User;

public class SignUpDialog extends DialogFragment {

    private DialogToActivityInteraction dialogToActivityInteraction;

    private TextView errorTextView;

    private Handler handler = new Handler();
    private Runnable hideErrorsRunnable = new Runnable() {
        @Override
        public void run() {
            errorTextView.setVisibility(View.INVISIBLE);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sign_up, null);

        final boolean[] fieldValidated = new boolean[5];

        final EditText usernameEditText = view.findViewById(R.id.username_edittext);
        final EditText emailEditText = view.findViewById(R.id.email_edittext);
        final EditText phoneEditText = view.findViewById(R.id.phone_number_edittext);
        final EditText passwordEditText = view.findViewById(R.id.password_edittext);
        final EditText confirmPasswordEditText = view.findViewById(R.id.confirm_password_edittext);
        errorTextView = view.findViewById(R.id.error_textview);

        view.findViewById(R.id.signUp_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringBuilder error = new StringBuilder();
                boolean allValid = true;
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                String confirmPassword = confirmPasswordEditText.getText().toString();
                String phoneNumber = phoneEditText.getText().toString();
                String userName = usernameEditText.getText().toString();

                if (!Validator.isUserNameValid(userName)) {
                    allValid = false;
                    error.append(getString(R.string.invalid_username) + "\n");
                }
                if (!Validator.isEmailValid(email)) {
                    allValid = false;
                    error.append(getString(R.string.invalid_email) + "\n");
                }
                if (!Validator.isPasswordValid(password)) {
                    allValid = false;
                    error.append(getString(R.string.invalid_password) + "\n");
                }
                if (!Validator.isPhoneNumberValid(phoneNumber)) {
                    allValid = false;
                    error.append(getString(R.string.invalid_phone_number) + "\n");
                }
                if (!password.equals(confirmPassword)) {
                    allValid = false;
                    error.append(getString(R.string.password_match_error));
                }
                if (allValid) {
                    dialogToActivityInteraction.insertUser(new User(email, userName, password, phoneNumber));
                } else {
                    //show error textview
                    errorTextView.setText(error.toString());
                    errorTextView.setVisibility(View.VISIBLE);
                    handler.postDelayed(hideErrorsRunnable, 1500);
                }
            }
        });
        return view;
    }

    public void setDialogToActivityInteraction(DialogToActivityInteraction dialogToActivityInteraction) {
        this.dialogToActivityInteraction = dialogToActivityInteraction;
    }

    interface DialogToActivityInteraction {
        void insertUser(User user);
    }

}
