package com.example.task1.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.task1.R;
import com.example.task1.model.User;
import com.example.task1.viewmodel.LoginSignupViewModel;

public class MainActivity extends AppCompatActivity {

    private LoginSignupViewModel viewModel;
    private EditText passwordEditText, emailEditText;
    private TextView errorTextView;

    private Handler handler = new Handler();
    private Runnable hideErrorsRunnable = new Runnable() {
        @Override
        public void run() {
            errorTextView.setVisibility(View.INVISIBLE);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(LoginSignupViewModel.class);
        emailEditText = findViewById(R.id.email_edittext);
        passwordEditText = findViewById(R.id.password_edittext);
        errorTextView = findViewById(R.id.error_tv);

        attachObservers();
    }

    private void attachObservers() {
        viewModel.getErrors().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                errorTextView.setText(s);
                errorTextView.setVisibility(View.VISIBLE);
                handler.postDelayed(hideErrorsRunnable, 1500);
            }
        });
        viewModel.getIsDataValid().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isDataValid) {
                if (isDataValid) {
                    //go to home activity show toast as sign up successful
                    showToast("sign in successful");
                    goToHome(viewModel.getUser());
                }
            }
        });
    }

    private void goToHome(User user) {
        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
        intent.putExtra("email", user.getEmail());
        intent.putExtra("password", user.getPassword());
        startActivity(intent);
        finish();
    }

    private void showToast(String info) {
        Toast.makeText(this, info, Toast.LENGTH_SHORT).show();
    }

    public void handleSignIn(View view) {
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        viewModel.validateAndInsert(email, password);
    }

    public void showSignUpDialog(final View view) {
        SignUpDialog.DialogToActivityInteraction dialogToActivityInteraction = new SignUpDialog.DialogToActivityInteraction() {
            @Override
            public void insertUser(User user) {
                try {
                    viewModel.insertUser(user);
                    showToast("sign up successful.");
                    goToHome(user);
                } catch (Exception e) {
                    //user already exists
                    showToast(getString(R.string.user_already_exists));
                    e.printStackTrace();
                }
            }
        };
        SignUpDialog signUpDialog = new SignUpDialog();
        signUpDialog.setDialogToActivityInteraction(dialogToActivityInteraction);
        signUpDialog.show(getSupportFragmentManager(), "signUpDialog");

    }

}
