package com.example.task1.view;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.task1.R;
import com.example.task1.helper.Validator;

import org.w3c.dom.Text;

public class BottomSheetDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    private BottomSheetDialogInteraction listener;
    private EditText usernameEditText;
    private EditText phoneEditText;
    private TextView errorTextView;

    private Handler handler = new Handler();
    private Runnable hideErrorsRunnable = new Runnable() {
        @Override
        public void run() {
            errorTextView.setVisibility(View.INVISIBLE);
        }
    };
    private Button saveButton, editButton, deleteButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();

        boolean showEmptyField = bundle.getBoolean("showEmptyField");

        View view = inflater.inflate(R.layout.add_user_bottom_sheet, container, false);

        usernameEditText = view.findViewById(R.id.username_edittext);
        phoneEditText = view.findViewById(R.id.phone_number_edittext);
        errorTextView = view.findViewById(R.id.error_tv);

        saveButton = view.findViewById(R.id.save_btn);
        editButton = view.findViewById(R.id.edit_btn);
        deleteButton = view.findViewById(R.id.delete_btn);

        if (!showEmptyField) {
            String userName = bundle.getString("username");
            String phone = bundle.getString("phone");
            usernameEditText.setText(userName);
            phoneEditText.setText(phone);
            editButton.setOnClickListener(this);
            deleteButton.setOnClickListener(this);
            saveButton.setVisibility(View.INVISIBLE);
        } else {
            deleteButton.setVisibility(View.INVISIBLE);
            editButton.setVisibility(View.INVISIBLE);
            saveButton.setOnClickListener(this);
        }

        return view;
    }

    public void setListener(BottomSheetDialogInteraction listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        String userName = usernameEditText.getText().toString();
        String phone = phoneEditText.getText().toString();
        if (!isDataValid(userName,phone))
            return;
        switch (view.getId()) {
            case R.id.edit_btn:
                listener.onEditBtnClicked(userName,phone);
                break;

            case R.id.delete_btn:
                listener.onDeleteBtnClicked(userName,phone);
                break;

            case R.id.save_btn:
                listener.onSaveBtnClicked(userName,phone);
                break;
        }
    }

    private boolean isDataValid(String userName, String phone) {
        boolean isUserNameValid = Validator.isUserNameValid(userName);
        boolean isPhoneNumberValid = Validator.isPhoneNumberValid(phone);

        StringBuilder sb = new StringBuilder();

        if (!isUserNameValid && !isPhoneNumberValid) {
            sb.append(getString(R.string.invalid_username) + "\n");
            sb.append(getString(R.string.invalid_phone_number));
        } else if (!isUserNameValid) {
            sb.append(getString(R.string.invalid_username));
        } else if (!isPhoneNumberValid) {
            sb.append(getString(R.string.invalid_phone_number) + "\n");
        } else {
            return true;
        }
        errorTextView.setText(sb.toString());
        errorTextView.setVisibility(View.VISIBLE);
        handler.postDelayed(hideErrorsRunnable,1500);
        return false;
    }

    interface BottomSheetDialogInteraction {
        void onSaveBtnClicked(String username, String phone);

        void onEditBtnClicked(String username, String phone);

        void onDeleteBtnClicked(String username, String phone);
    }
}
