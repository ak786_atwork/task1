package com.example.task1.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.task1.R;
import com.example.task1.adapters.RecyclerViewAdapter;
import com.example.task1.model.User;
import com.example.task1.model.UserConnections;
import com.example.task1.viewmodel.HomeViewModel;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity  {

    private ArrayList<UserConnections> userArrayList;
    private HomeViewModel viewModel;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;

    private User user;
    private int listItemPosition;
    private BottomSheetDialog bottomSheetDialog;

    private BottomSheetDialog.BottomSheetDialogInteraction listener = new BottomSheetDialog.BottomSheetDialogInteraction() {
        @Override
        public void onSaveBtnClicked(String username, String phone) {
//            showToast("save clicked username = "+username+" phone = "+phone);
            userArrayList.add(new UserConnections(username,phone));
            adapter.setUsers(userArrayList);
            handleDialog();
        }

        @Override
        public void onEditBtnClicked(String username, String phone) {
//            showToast("edit clicked username = "+username+" phone = "+phone);
            userArrayList.get(listItemPosition).setUserName(username);
            userArrayList.get(listItemPosition).setMobileNumber(phone);
            adapter.setUsers(userArrayList);
            handleDialog();
        }

        @Override
        public void onDeleteBtnClicked(String username, String phone) {
//            showToast("delete clicked username = "+username+" phone = "+phone);
            userArrayList.remove(listItemPosition);
            adapter.notifyDataSetChanged();
            handleDialog();
        }
    };

    private void handleDialog() {
        if (bottomSheetDialog != null && !bottomSheetDialog.isRemoving()) {
            bottomSheetDialog.dismiss();
        }
    }

    private RecyclerViewAdapter.OnItemClickedListerner itemClickedListerner = new RecyclerViewAdapter.OnItemClickedListerner() {
        @Override
        public void onItemClicked(int position) {
            listItemPosition = position;
            bottomSheetDialog = new BottomSheetDialog();
            Bundle bundle = new Bundle();
            bundle.putBoolean("showEmptyField",false);
            bundle.putString("username", userArrayList.get(position).getUserName());
            bundle.putString("phone", userArrayList.get(position).getMobileNumber());
            bottomSheetDialog.setArguments(bundle);
            bottomSheetDialog.setListener(listener);
            bottomSheetDialog.show(getSupportFragmentManager(),"editDeleteUserFragment");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        Intent intent = getIntent();
        if (intent != null) {
            String email = intent.getStringExtra("email");
            String password = intent.getStringExtra("password");
            user = viewModel.getUser(email,password);
            getSupportActionBar().setTitle("Hi, "+user.getUserName());

        }

        recyclerView = findViewById(R.id.contact_recyclerview);
        userArrayList = user != null ? user.getUserContacts() : new ArrayList<UserConnections>();
        adapter = new RecyclerViewAdapter(userArrayList,itemClickedListerner);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog = new BottomSheetDialog();
                Bundle bundle = new Bundle();
                bundle.putBoolean("showEmptyField",true);
                bottomSheetDialog.setArguments(bundle);
                bottomSheetDialog.setListener(listener);
                bottomSheetDialog.show(getSupportFragmentManager(),"addUserFragment");
            }
        });

        viewModel.getUsersList().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            goToLoginScreen();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void goToLoginScreen() {
        Intent intent = new Intent(HomeActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }


    private void showToast(String info) {
        Toast.makeText(this, info, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPause() {
        user.setUserContacts(userArrayList);
        viewModel.updateUserContactsList(user);
        super.onPause();
    }
}
