package com.example.task1.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.task1.R;
import com.example.task1.model.UserConnections;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private List<UserConnections> users;
    private OnItemClickedListerner listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView username,phone;
        public View view;
        public MyViewHolder(View view) {
            super(view);
            this.view = view;
            username =  view.findViewById(R.id.username_textview);
            phone =  view.findViewById(R.id.phone_number_textview);
        }
    }


    public RecyclerViewAdapter(List<UserConnections> users,OnItemClickedListerner listener) {
        this.users = users;
        this.listener = listener;
    }

    public void setUsers(List<UserConnections> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        UserConnections user = users.get(position);
        holder.phone.setText(user.getMobileNumber());
        holder.username.setText(user.getUserName());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClicked(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return users.size();
    }


    public interface OnItemClickedListerner{
        void onItemClicked(int position);
    }
}