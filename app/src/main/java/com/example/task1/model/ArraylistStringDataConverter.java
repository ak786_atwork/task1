package com.example.task1.model;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class ArraylistStringDataConverter {
    @TypeConverter
    public String getStringFromArraylist(ArrayList<UserConnections> users) {
        return new Gson().toJson(users);
    }

    @TypeConverter
    public ArrayList<UserConnections> getArraylistFromString(String usersDataString) {
        ArrayList<UserConnections> users = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(usersDataString);
            for (int i = 0; i < jsonArray.length(); i++) {
                users.add(new Gson().fromJson(jsonArray.getString(i), UserConnections.class));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return users;
    }
}
