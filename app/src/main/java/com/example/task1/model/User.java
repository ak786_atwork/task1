package com.example.task1.model;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.ArrayList;

@Entity(tableName = "users")
public class User {

    @NonNull
    @PrimaryKey
    private String email;

    @NonNull
    private String userName;
    @NonNull
    private String password;
    private String mobileNumber;

    @TypeConverters(ArraylistStringDataConverter.class)
    private ArrayList<UserConnections> userContacts;

    public User(){}

    public User(@NonNull String email, String userName, String password, String mobileNumber) {
        this.email = email;
        this.userName = userName;
        this.password = password;
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public ArrayList<UserConnections> getUserContacts() {
        return userContacts;
    }

    public void setUserContacts(ArrayList<UserConnections> userContacts) {
        this.userContacts = userContacts;
    }
}
